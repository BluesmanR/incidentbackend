insert into users(password,user) values ('12345','admin');
insert into users(password,user) values ('1234','admin2');
insert into users(password,user) values ('1234','admin3');

insert into incidents(title,description,stats,status,start) values ('Incident example','This is an incident example',
'','Open',CURRENT_TIMESTAMP());
insert into incidents(title,description,stats,status,start) values ('The SMS are down ','The sms are down in those countries: Mexico, Panama, Peru',
'','Open',CURRENT_TIMESTAMP());
insert into incidents(title,description,stats,status,start,finish) values ('Something wrong in the package ','Balbalbla lorem ipsum something wrong with someone aasdasd',
'','Closed',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());

insert into updates(id_incident,type,update_content,updateDT) values (1,'Investigation','we are doing a research about the problem',CURRENT_TIMESTAMP());
insert into updates(id_incident,type,update_content,updateDT) values (1,'Update','Updating things',CURRENT_TIMESTAMP());


