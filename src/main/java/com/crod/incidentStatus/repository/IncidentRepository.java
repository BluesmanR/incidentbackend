package com.crod.incidentStatus.repository;

import com.crod.incidentStatus.data.Incident;
import org.springframework.data.repository.CrudRepository;

public interface IncidentRepository extends CrudRepository<Incident, Integer> {
}
