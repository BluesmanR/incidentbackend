package com.crod.incidentStatus.repository;

import com.crod.incidentStatus.data.Update;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UpdateRepository extends CrudRepository<Update,Integer> {

    List<Update> findByIdIncident(Integer idIncident);
}
