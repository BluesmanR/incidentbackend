package com.crod.incidentStatus.repository;

import com.crod.incidentStatus.data.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    User findByUserAndPassword(String username, String password);
}
