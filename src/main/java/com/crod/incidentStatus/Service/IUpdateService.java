package com.crod.incidentStatus.Service;

import com.crod.incidentStatus.data.Incident;
import com.crod.incidentStatus.data.Update;

import java.util.List;
import java.util.Optional;

public interface IUpdateService {

    List<Update> getUpdates();
    List<Update> getUpdatesByIdIncident(Integer id);
    Update saveUpdate(Update update);
    Optional<Update> getUpdateById(Integer id);
    void deleteUpdate(Integer id);
}
