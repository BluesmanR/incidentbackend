package com.crod.incidentStatus.Service;

import com.crod.incidentStatus.data.User;
import com.crod.incidentStatus.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService  implements IUserService{

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getUsers() {
        return  (List<User>) userRepository.findAll();
    }

    @Override
    public User getUserByUsernameAndPassword(String username, String password) {
        return userRepository.findByUserAndPassword(username,password);
    }
}
