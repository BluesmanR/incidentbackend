package com.crod.incidentStatus.Service;

import com.crod.incidentStatus.data.Incident;

import java.util.List;
import java.util.Optional;

public interface IIncidentService {

    List<Incident> getIncidents();
    Incident saveIncident(Incident incident);
    Optional<Incident> getIncidentById(Integer id);
    void deleteIncident(Integer id);
}
