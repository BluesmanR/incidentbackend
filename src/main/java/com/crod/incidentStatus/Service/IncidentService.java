package com.crod.incidentStatus.Service;

import com.crod.incidentStatus.data.Incident;
import com.crod.incidentStatus.repository.IncidentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class IncidentService implements IIncidentService {

    @Autowired
    private IncidentRepository incidentRepository;


    @Override
    public List<Incident> getIncidents() {
        return (List<Incident>) incidentRepository.findAll();
    }

    @Override
    public Incident saveIncident(Incident incident) {
        return incidentRepository.save(incident);
    }

    @Override
    public Optional<Incident> getIncidentById(Integer id) {
        return incidentRepository.findById(id);
    }

    @Override
    public void deleteIncident(Integer id) {
        incidentRepository.deleteById(id);
    }
}
