package com.crod.incidentStatus.Service;

import com.crod.incidentStatus.data.User;

import java.util.List;
import java.util.Optional;

public interface IUserService {

    List<User> getUsers();
    User getUserByUsernameAndPassword(String username, String password);

}
