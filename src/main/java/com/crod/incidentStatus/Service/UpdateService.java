package com.crod.incidentStatus.Service;

import com.crod.incidentStatus.data.Incident;
import com.crod.incidentStatus.data.Update;
import com.crod.incidentStatus.repository.UpdateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UpdateService  implements  IUpdateService{

    @Autowired
    private UpdateRepository updateRepository;

    @Override
    public List<Update> getUpdates() {
        return (List<Update>) updateRepository.findAll();
    }

    @Override
    public List<Update> getUpdatesByIdIncident(Integer id) {
        return updateRepository.findByIdIncident(id);
    }


    @Override
    public Update saveUpdate(Update update) {
        return updateRepository.save(update);
    }

    @Override
    public Optional<Update> getUpdateById(Integer id) {
        return updateRepository.findById(id);
    }

    @Override
    public void deleteUpdate(Integer id) {
        updateRepository.deleteById(id);
    }
}
