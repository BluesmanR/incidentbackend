package com.crod.incidentStatus.controller;

import com.crod.incidentStatus.Service.IUserService;
import com.crod.incidentStatus.data.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:4200/")
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private IUserService userService;

    @GetMapping("/")
    public List<User> getUsers(){
        return userService.getUsers();
    }

    @PostMapping("/login")
    public @ResponseBody ResponseEntity<?> login(@RequestBody User user){
        User userRes = userService.getUserByUsernameAndPassword(user.getUser(),user.getPassword());
        Map<String,Boolean> result = new HashMap<String, Boolean>();
        result.put("login",false);

        if(userRes != null){
            result.put("login",true);
        }

        return new ResponseEntity<Map>(result, HttpStatus.OK);

    }
}
