package com.crod.incidentStatus.controller;

import com.crod.incidentStatus.Service.IUpdateService;
import com.crod.incidentStatus.data.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200/")
@RestController
@RequestMapping("update")
public class UpdateController {

    @Autowired
    private IUpdateService updateService;

    @GetMapping("/")
    public List<Update> getUpdate(){ return  updateService.getUpdates();}

    @GetMapping("/{id}")
    public Update getUpdate(@PathVariable Integer id){ return  updateService.getUpdateById(id).get();}

    @GetMapping("/incident/{id}")
    public List<Update> getUopdatesByIncident(@PathVariable Integer id){ return updateService.getUpdatesByIdIncident(id);}
    @PostMapping("/")
    public @ResponseBody
    ResponseEntity<String> saveUpdate(@RequestBody Update update){
        try{
            updateService.saveUpdate(update);
        }catch (Exception ex){
            return  new ResponseEntity<String>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<String>("", HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public @ResponseBody ResponseEntity<?> updateUpdate(@RequestBody Update update, @PathVariable Integer id){
        try{

            updateService.saveUpdate(update);
        }catch (Exception ex){
            return  new ResponseEntity<String>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<String>("", HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public @ResponseBody ResponseEntity<?> deleteUpdate(@PathVariable Integer id){
        updateService.deleteUpdate(id);
        return new ResponseEntity<String>("", HttpStatus.OK);
    }

}
