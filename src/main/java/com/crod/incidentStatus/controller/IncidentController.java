package com.crod.incidentStatus.controller;

import com.crod.incidentStatus.Service.IIncidentService;
import com.crod.incidentStatus.Service.IUpdateService;
import com.crod.incidentStatus.data.Incident;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200/")
@RestController
@RequestMapping("incident")
public class IncidentController {

    @Autowired
    private IIncidentService incidentService;

    @Autowired
    private IUpdateService updateService;

    @GetMapping("/")
    public List<Incident> getIncidents(){ return  incidentService.getIncidents();}

    @GetMapping("/{id}")
    public Incident getIncident(@PathVariable Integer id){ return  incidentService.getIncidentById(id).get();}

    @PostMapping("/")
    public @ResponseBody ResponseEntity<String> saveIncident(@RequestBody Incident incident){
        try{

            incidentService.saveIncident(incident);
        }catch (Exception ex){
            return  new ResponseEntity<String>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<String>("", HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public @ResponseBody ResponseEntity<?> updateIncident(@RequestBody Incident incident, @PathVariable Integer id){
        try{
            Optional<Incident> opIncident = incidentService.getIncidentById(id);
            if(!opIncident.isPresent()){
                return  ResponseEntity.notFound().build();
            }
            Incident originalIncident = opIncident.get();

            originalIncident.setStats(incident.getStats());
            originalIncident.setFinish(incident.getFinish());
            originalIncident.setStatus(incident.getStatus());
            originalIncident.setDescription(incident.getDescription());
            originalIncident.setTitle(incident.getTitle());



            incidentService.saveIncident(originalIncident);
        }catch (Exception ex){
            return  new ResponseEntity<String>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<String>("", HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public @ResponseBody ResponseEntity<?> deleteIncident(@PathVariable Integer id){
        Incident incident = incidentService.getIncidentById(id).get();
        incident.getUpdates().stream().forEach(x -> updateService.deleteUpdate(x.getIdUpdate()));
        incidentService.deleteIncident(id);

        return new ResponseEntity<String>("", HttpStatus.OK);
    }



}
