package com.crod.incidentStatus.data;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "updates")
public class Update {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idUpdate;

    @Column(name = "type")
    private String type;

    @Column(name = "updateContent")
    private String updateContent;

    @Column(name = "updateDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDt;

    //@OneToOne
    //@JoinColumn(name = "id_incident")
    private Integer idIncident;

}
