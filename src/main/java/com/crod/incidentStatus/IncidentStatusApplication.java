package com.crod.incidentStatus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IncidentStatusApplication {

	public static void main(String[] args) {
		SpringApplication.run(IncidentStatusApplication.class, args);
	}

}
